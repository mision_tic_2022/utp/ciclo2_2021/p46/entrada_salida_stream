import java.io.IOException;
import java.io.PrintWriter;

public class GestionArchivos {
    public static void main(String[] args) throws Exception {
        // Declarar una matriz
        // int[][] numeros = new int[5][5];
        int[][] numeros = { { 1, 2, 3, 4, 5 }, { 6, 7, 8, 9, 10 }, { 11, 12, 13, 14, 15 } };
        // Crear objeto para manipular salida de datos
        // String ruta_fichero = "./numeros.txt";
        
        try (PrintWriter salida = new PrintWriter("./numeros.txt")){
            // Recorrer la matriz números
            for (int i = 0; i < numeros.length; i++) {
                // Iterar columnas
                for (int j = 0; j < numeros[i].length; j++) {
                    salida.print(numeros[i][j] + "\t");
                }
                salida.println("");
            }
        } catch (IOException e) {
            // TODO: handle exception
            System.err.println("Error al guardar los datos");
        }

        // salida.close();

    }

}
